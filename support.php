<?php
require_once( 'Connections/transcribe.php' );
//include( "includes/appstatus.php" );
include( "functions.php" );
//include( "includes/nav-query.php" );

$date = date( "Y-m-d H:i:s" );

if ( ( isset( $_POST[ "MM_insert" ] ) ) && ( $_POST[ "MM_insert" ] == "form1" ) ) {

    //var_dump($_POST);

    $success = true;

    if ( !isset( $_POST[ 'support' ] )) {
        $status = "Please enter your feedback.";
        $success = false;
    }
    else
    {
        $success = true;
        
        $ticket = generateRandomTicket(5);
        
        $support = $_POST[ 'support' ];
        
        $insertSQL = sprintf( "INSERT INTO support (message, ticket, userid, datecreated) VALUES (%s, %s, %s, %s)",
        GetSQLValueString( $support, "text" ),
        GetSQLValueString( $ticket, "text" ),
        GetSQLValueString( $_POST[ 'uid' ], "int" ),
        GetSQLValueString( $date, "date" ) );

        mysql_select_db( $database_transcribe, $transcribe );
        $Result1 = mysql_query( $insertSQL, $transcribe )or die( mysql_error() );

        $status = "Thank you! Your support ticket #{$ticket} has been submitted. We will contact you shortly.";
        
        
        $message = $support;

        $to = "aiscribedev@gmail.com";
        $subject = "AIScribe support ticket {}";
        $html = $message;
        $text = $message;
        $from = "noreply@myaiscribe.com";

        include("send-email.php");
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="boilerplate.css">
    <link rel="stylesheet" href="maintenance.css">
    <title>Support - AIScribe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
</head>
<body>
    <div id="primaryContainer" class="primaryContainer clearfix">
        <div id="headerBG" class="clearfix">
            <span style="font-size:30px;cursor:pointer"><img id="navIcon" name="navIcon" src="img/Hamburger_icon.png" class="image"/></span>
            <a href="index.php"><img id="logo" src="img/logo.png" class="image"/></a>
        </div>
        <div id="titleDiv" class="clearfix">
            <div id="headerTxtBG" class="clearfix">
                <p id="headerLbl">Support</p>
            </div>
        </div>
        <div id="contentBG" class="clearfix">
            <form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">

                <p>&nbsp;</p>
               
                <?php if(isset($status) && (strpos($status, 'has been submitted') !== false)) { 

                echo $status;


                } else { ?>
                
                
                <table width="100%" cellpadding="5" cellspacing="5">
                    <tbody>
                        <?php if(isset($status)) { ?>
                        <tr>
                            <td width="93%" style="color: red">
                                <?php echo $status; ?>
                            </td>
                        </tr>

                        <?php } ?>
                        <tr>
                          <td>How can we help?</td>
                        </tr>
                        <tr>
                            <td><textarea name="support" class="support" id="support"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="hidden" name="MM_insert" value="form1">
                                <input type="submit" name="submit" id="submit" value="Submit">
                                <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']?>">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <?php } ?>
            </form>
        </div>
    </div>
</body>
</html>