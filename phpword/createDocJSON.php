<?php

require_once 'vendor/autoload.php';

//$_POST['input'] = "hi";
//$_POST['filename'] = "test";

if(isset($_POST['input']))
{
    $phpWord = new \PhpOffice\PhpWord\PhpWord();

    $section = $phpWord->addSection();
    
    $input = $_POST['input'];
    
    if(isset($_POST['lang']))
    {
        $input = $_POST['lang'] . "<br><br>" . $input;
    }

    $fontStyle = new \PhpOffice\PhpWord\Style\Font();
    $fontStyle->setBold(true);
    $fontStyle->setName('Arial');
    $fontStyle->setSize(16);
    $myTextElement = $section->addText($input);
    $myTextElement->setFontStyle($fontStyle);

    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save("../uploads/{$_POST['filename']}.docx");
    
    $response = [ "status" => "docx created successfully"];    
}

echo "{\"data\":";
echo "{\"docData\":";
echo json_encode( $response );
echo "}";
echo "}";




?>