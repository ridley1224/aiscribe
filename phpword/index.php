<?php

require_once 'vendor/autoload.php';


$phpWord = new \PhpOffice\PhpWord\PhpWord();


$section = $phpWord->addSection();

$fontStyle = new \PhpOffice\PhpWord\Style\Font();
$fontStyle->setBold(true);
$fontStyle->setName('Arial');
$fontStyle->setSize(16);
$myTextElement = $section->addText('Hello World');
$myTextElement->setFontStyle($fontStyle);

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('../uploads/helloWorld.docx');

?>