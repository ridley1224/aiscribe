
<?php

require_once('Connections/transcribe.php'); 
include("functions.php");

 if(!isset($_POST['mobile']))
{
    $_POST[ 'userid' ] = $_SESSION[ 'userid' ];
}

//https://cloud.ibm.com/apidocs/speech-to-text#add-a-corpus
//https://cloud.ibm.com/docs/services/speech-to-text?topic=speech-to-text-manageGrammars#listGrammars

//$_POST['mobile'] = true;

//$_POST['customizationid'] = "ff8b45ef-cd5d-4299-8483-c84761526ebd";
//$_POST['filename'] = "rom";
//$testfile = "uploads/rom transcription.txt";

$date = date("Y-m-d H:i:s");

mysql_select_db( $database_transcribe, $transcribe );

//$query_rsCorpusInfo = sprintf( "SELECT filename FROM corpus WHERE filename = %s", GetSQLValueString( $_POST['filename'], "text" ) );
//$rsCorpusInfo = mysql_query( $query_rsCorpusInfo, $transcribe )or die( mysql_error() );
//$row_rsCorpusInfo = mysql_fetch_assoc( $rsCorpusInfo );
//$totalRows_rsCorpusInfo = mysql_num_rows( $rsCorpusInfo );
//
//if($totalRows_rsCorpusInfo > 0)
//{
//    if(isset($_POST['mobile']))
//    {
//        $myObj = new stdClass;
//        $myObj->status = "Corpus name {$_POST['filename']} already taken. Please select another name.";
//
//        echo "{\"data\":";
//        echo "{\"corpusData\":";
//        echo json_encode( $myObj );
//        echo "}";
//        echo "}";
//    }
//    else
//    {
//        echo $status;
//    }
//    
//    return;
//}

$query_rsKeyInfo = sprintf( "SELECT apikey FROM apikeys WHERE service = %s AND active = 1", GetSQLValueString( "stt", "text" ) );
$rsKeyInfo = mysql_query( $query_rsKeyInfo, $transcribe )or die( mysql_error() );
$row_rsKeyInfo = mysql_fetch_assoc( $rsKeyInfo );


$apiKey = $row_rsKeyInfo['apikey'];

if(isset($_POST['filename']) && isset($_POST['customizationid']) && isset($apiKey))
{
    $customizationid = $_POST['customizationid'];
    $a = explode(".",$_POST['filename']);
    $filename = $a[0];
    
    //$_POST['filename'] = urlencode("rom");
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://iam.bluemix.net/identity/token",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "grant_type=urn%3Aibm%3Aparams%3Aoauth%3Agrant-type%3Aapikey&apikey={$apiKey}",
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Postman-Token: 1d378144-7f93-4d72-8b2d-3d775883d3f3",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {

        //$status = "cURL Error1 #:" . $err;
        $status = $err;
        
        if(isset($_POST['mobile']))
        {
            $myObj = new stdClass;
            $myObj->status = $status;

            echo "{\"data\":";
            echo "{\"corpusData\":";
            echo json_encode( $myObj );
            echo "}";
            echo "}";
        }
        else
        {
            echo $status;
        }

    } else {

        //echo "token response: $response";

        $decodedData = json_decode($response);

        //var_dump($decodedData);

        $token = $decodedData->access_token;
        
//        $file = $_FILES['file']['tmp_name'];
//        
//        $data = file_get_contents($file);
        
        
//        if(move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_FILES['file']['name']))
//        {
//            $data = file_get_contents('uploads/' . $_FILES['file']['name']);
//        }
        
        if(isset($testfile))
        {
            $path = $testfile;
        }
        else
        {
            $path = $_FILES['file']['tmp_name'];
            //$path = $_POST['file'];
            //$path = $_POST['file'];
        }
//        
        $data = file_get_contents($path);
//                
        //$data = file_get_contents("uploads/{$filename}");

        //echo "<p>token: {$token}</p>";

        $convert = true;

        if($convert == true)
        {
            $curl2 = curl_init();
            
            //echo "url: {$url}";
            
            $formatted = urlencode($filename);
            
            $url = "https://stream.watsonplatform.net/speech-to-text/api/v1/customizations/{$customizationid}/corpora/{$formatted}?allow_overwrite=true";

            curl_setopt_array($curl2, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $data,
              CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$token}",
                "cache-control: no-cache"                  
              ),
            ));

            $response2 = curl_exec($curl2);
            $err2 = curl_error($curl2);

            curl_close($curl2);

            if ($err2) {

              //$status = "cURL Error2 #:" . $err2;
              $status = $err2;
        
                if(isset($_POST['mobile']))
                {
                    $myObj = new stdClass;
                    $myObj->status = $status;

                    echo "{\"data\":";
                    echo "{\"corpusData\":";
                    echo json_encode( $myObj );
                    echo "}";
                    echo "}";
                }
                else
                {
                    echo $status; // . " file1: " . $_FILES['file']['tmp_name'];
                }

            } else {

                //echo "<p>convert response: {$response2}</p>";
                
                //var_dump($response2);
                
                $json = json_decode($response2);
                
                //["code"]=> int(400)

                $code = $json->code;
                
                if(!isset($code))
                {
                    //insert file info into DB
                    
                    $insertSQL = sprintf( "INSERT INTO corpus (filename, customizationid, content, userid, status, datecreated) VALUES (%s, %s, %s, %s, %s, %s)",
                    GetSQLValueString( $filename, "text" ),
                    GetSQLValueString( $customizationid, "text" ),
                    GetSQLValueString( $data, "text" ),
                    GetSQLValueString( $_POST[ 'userid' ], "int" ),
                    GetSQLValueString( 0, "int" ),
                    GetSQLValueString( $date, "date" ) );

                    mysql_select_db( $database_transcribe, $transcribe );
                    $Result1 = mysql_query( $insertSQL, $transcribe )or die( mysql_error() );

                    $status = "corpus created successfully";
                    $corpusid = mysql_insert_id();
                    
                    if(isset($_POST['mobile']))
                    {
                        $myObj = new stdClass;
                        $myObj->status = $status;
                        $myObj->corpusid = blankNull(strval($corpusid));

                        echo "{\"data\":";
                        echo "{\"corpusData\":";
                        echo json_encode( $myObj );
                        echo "}";
                        echo "}";
                    }
                    else
                    {
                        //echo $status . " file2: " . $_FILES['file']['tmp_name'];
                        echo $status;
                    }
                                        
//                    //automatically train custom model
//                    
//                    $curl3 = curl_init();
//
//                    curl_setopt_array($curl3, array(
//                      CURLOPT_URL => "https://stream.watsonplatform.net/speech-to-text/api/v1/customizations/{$customizationid}/train",
//                      CURLOPT_RETURNTRANSFER => true,
//                      CURLOPT_ENCODING => "",
//                      CURLOPT_MAXREDIRS => 10,
//                      CURLOPT_TIMEOUT => 30,
//                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                      CURLOPT_CUSTOMREQUEST => "POST",
//                      CURLOPT_HTTPHEADER => array(
//                        "Accept: */*",
//                        "Authorization: Bearer {$token}",
//                        "Cache-Control: no-cache",
//                        "Connection: keep-alive",
//                        "Host: stream.watsonplatform.net",
//                        "Postman-Token: 31a46112-5376-4dc0-8668-34b302fc0e0f,9dfc2df8-0074-4eaa-b075-c8945b85b400",
//                        "User-Agent: PostmanRuntime/7.15.0",
//                        "accept-encoding: gzip, deflate",
//                        "cache-control: no-cache"
//                      ),
//                    ));
//
//                    $response3 = curl_exec($curl);
//                    $err3 = curl_error($curl3);
//
//                    curl_close($curl);
//
//                    if ($err3) {
//                      $status =  "cURL train Error #:" . $err3;
//                    } else {
//                      //echo $response3;
//                        
//                        $trainStatus = "model trained successfully";
//                        
//                        if(isset($_POST['mobile']))
//                        {
//                            $myObj = new stdClass;
//                            $myObj->status = $status;
//                            $myObj->trainStatus = $trainStatus;
//                            $myObj->corpusid = blankNull(strval($corpusid));
//
//                            echo "{\"data\":";
//                            echo "{\"corpusData\":";
//                            echo json_encode( $myObj );
//                            echo "}";
//                            echo "}";
//                        }
//                        else
//                        {
//                            //echo $status . " file2: " . $_FILES['file']['tmp_name'];
//                            echo $status;
//                        }
//                    }
                }
                else
                {
                    $status = $json->error. " code: " . $code;
                    
                    if(isset($_POST['mobile']))
                    {
                        $myObj = new stdClass;
                        $myObj->status = $status;
                        $myObj->code = $json->code;

                        echo "{\"data\":";
                        echo "{\"corpusData\":";
                        echo json_encode( $myObj );
                        echo "}";
                        echo "}";
                    }
                    else
                    {
                        echo $status . " file3: {$filename} path: " . $_FILES['file']['tmp_name'];
                    }
                }
            }
        }
    }
}
else
{
    $status = "missing params";
    
    if(isset($_POST['mobile']))
    {
        $myObj = new stdClass;
        $myObj->status = $status;

        echo "{\"data\":";
        echo "{\"corpusData\":";
        echo json_encode( $myObj );
        echo "}";
        echo "}";
    }
    else
    {
        echo $status;
    }
}

?>      