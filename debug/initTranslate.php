<?php
require_once( 'Connections/transcribe.php' );

//STT token api x4TKd2zCz0S6gQ9Zmc_kWFP5iFTwDBOWJCc3872RY7Vb ridleytech@gmail.com
//Translation token api psQT-I3WUvcqe469H0QpQyGKqxuAzLS1Lv-VsyBPeTjG ridleytech@gmail.com

include( "functions.php" );

//$_POST['lang'] = "en-es";

//$_POST['userid'] = "1";
//$_POST['docid'] = "14";
//$_POST['lang'] = "en-de";
//$_POST['input'] = "Lando was also absent from The Last Jedi (2017).During the early development of the film, director Rian Johnson briefly considered bringing back Lando as the codebreaker that Finn and Rose Tico seek in the coastal city of Canto Bight, but Lando was finally written out of the film's script, with the codebreaker role ultimately going to Benicio del Toro's character DJ.";


$userid = $_POST[ 'userid' ];
$lang = $_POST[ 'lang' ];
$displayLang = $_POST[ 'langDisplay' ];

if (isset($_POST[ 'file' ])) {
    
    //get file contents
    
    $fileName = $_POST[ 'fileName' ];
    $fileType = $_POST[ 'fileType' ];

    $input = file_get_contents($_POST[ 'file' ]);
    
} else {
    
    $docid = $_POST[ 'docid' ];
    $input = $_POST[ 'input' ];
}

//$apiKey = "psQT-I3WUvcqe469H0QpQyGKqxuAzLS1Lv-VsyBPeTjG"; //ridleytech@gmail.com

mysql_select_db( $database_transcribe, $transcribe );
$query_rsKeyInfo = sprintf( "SELECT apikey FROM apikeys WHERE service = %s AND active = 1", GetSQLValueString( "translate", "text" ) );
$rsKeyInfo = mysql_query( $query_rsKeyInfo, $transcribe )or die( mysql_error() );
$row_rsKeyInfo = mysql_fetch_assoc( $rsKeyInfo );

$apiKey = $row_rsKeyInfo['apikey'];

if ( isset( $userid ) && isset( $input ) && isset($apiKey)) {
    
    $starttime = date( "Y-m-d H:i:s" );
    $curl = curl_init();
    
    $time = time();
    
    mysql_select_db( $database_transcribe, $transcribe );
    $query_rsTokenInfo = sprintf( "SELECT token FROM authtokens WHERE userid = %s AND expiry < {$time}", GetSQLValueString( $userid, "int" ) );
    $rsTokenInfo = mysql_query( $query_rsTokenInfo, $transcribe )or die( mysql_error() );
    $row_rsTokenInfo = mysql_fetch_assoc( $rsTokenInfo );
    $totalRows_rsTokenInfo = mysql_num_rows( $rsTokenInfo );
    
    $convert = true;
    
    if($totalRows_rsTokenInfo)
    {
        $token = $row_rsTokenInfo['token'];
        $refreshtoken = $row_rsTokenInfo['refreshtoken'];

        //echo "<p>token: {$token}</p>";

        if ( $convert == true ) {
            
            $curl3 = curl_init();

            curl_setopt_array($curl3, array(
              CURLOPT_URL => "https://iam.bluemix.net/identity/token",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token={$refreshtoken}",
              CURLOPT_HTTPHEADER => array(
                "Authorization: Basic Yng6Yng=",
                "Postman-Token: 38bfc973-6cd5-4d94-bad0-8ce311963b54",
                "cache-control: no-cache"
              ),
            ));

            $response3 = curl_exec($curl3);
            $err3 = curl_error($curl3);

            curl_close($curl3);

            if ($err3) {
              //echo "cURL Error #:" . $err3;
            } else {
              //echo $response;
                
                $decodedData = json_decode( $response3 );

                $updateSQL = sprintf( "UPDATE authtokens SET expiry=%s WHERE token=%s",

                GetSQLValueString( $decodedData->expiry, "text" ),
                GetSQLValueString( $token , "text" ));

                mysql_select_db( $database_transcribe, $transcribe );
                $Result1 = mysql_query( $updateSQL, $transcribe )or die( mysql_error() );
                
                include("IBM-translate2.php");
            }            
        }
    }
    else
    {
        curl_setopt_array( $curl, array(
        CURLOPT_URL => "https://iam.bluemix.net/identity/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=urn%3Aibm%3Aparams%3Aoauth%3Agrant-type%3Aapikey&apikey={$apikey}",
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 1d378144-7f93-4d72-8b2d-3d775883d3f3",
            "cache-control: no-cache"
            ),
        ) );

        $response = curl_exec( $curl );
        $err = curl_error( $curl );

        curl_close( $curl );

        if ( $err ) {

            echo "<p>cURL Error1 #:" . $err . "</p>";

        } else {

            //echo "<br>response: {$response}<br>";
            
            $decodedData = json_decode( $response );

            //var_dump($decodedData);

            $token = $decodedData->access_token;
            $refreshtoken = $decodedData->refresh_token;
            $expiry = $decodedData->expiry;
            
             $insertSQL = sprintf("INSERT INTO authtokens (token, refreshtoken,expiry,datecreated) VALUES (%s, %s, %s, %s)",
                    GetSQLValueString(mysql_real_escape_string($token), "text"),
                    GetSQLValueString(mysql_real_escape_string($refreshtoken), "text"),
                    GetSQLValueString(mysql_real_escape_string($expiry), "text"),
                    GetSQLValueString(mysql_real_escape_string($starttime), "date"));

            mysql_select_db($database_transcribe, $transcribe);
            $Result2 = mysql_query($insertSQL, $transcribe) or die(mysql_error());	

            //echo "<p>token: {$token}</p>";

            if ( $convert == true ) {

                include("IBM-translate2.php");
            }
        }
    }
}

//$0.02 USD/MINUTE

?>