<?php

$devStatus = "dev";

require_once( 'Connections/transcribe.php' );

include( "functions.php" );
include( "en-de.php" );

//test write

$colname_rsModels = "-1";
if ( isset( $_GET[ 'userid' ] ) ) {
    $colname_rsModels = $_GET[ 'userid' ];
}

if ( isset( $_SESSION[ 'userid' ] ) ) {
    $colname_rsModels = $_SESSION[ 'userid' ];
}

$currentPage = $_SERVER[ "PHP_SELF" ];

$maxRows_rsModels = 20;
$pageNum_rsModels = 0;
if ( isset( $_GET[ 'pageNum_rsModels' ] ) ) {
    $pageNum_rsModels = $_GET[ 'pageNum_rsModels' ];
}
$startRow_rsModels = $pageNum_rsModels * $maxRows_rsModels;

mysql_select_db( $database_transcribe, $transcribe );
$query_rsModels = sprintf( "SELECT a.*, b.modelname2 FROM (SELECT * FROM custommodels WHERE userid = {$colname_rsModels} AND active = 1 ORDER by datecreated DESC) as a INNER JOIN (SELECT modelname as 'modelname2',code from modeloptions) as b on a.code = b.code" );

$query_limit_rsModels = sprintf( "%s LIMIT %d, %d", $query_rsModels, $startRow_rsModels, $maxRows_rsModels );
$rsModels = mysql_query( $query_limit_rsModels, $transcribe )or die( mysql_error() );
$row_rsModels = mysql_fetch_assoc( $rsModels );

if ( isset( $_GET[ 'totalRows_rsModels' ] ) ) {
    $totalRows_rsModels = $_GET[ 'totalRows_rsModels' ];
} else {
    $all_rsModels = mysql_query( $query_rsModels );
    $totalRows_rsModels = mysql_num_rows( $all_rsModels );
}
$totalPages_rsModels = ceil( $totalRows_rsModels / $maxRows_rsModels ) - 1;

$queryString_rsModels = "";
if ( !empty( $_SERVER[ 'QUERY_STRING' ] ) ) {
    $params = explode( "&", $_SERVER[ 'QUERY_STRING' ] );
    $newParams = array();
    foreach ( $params as $param ) {
        if ( stristr( $param, "pageNum_rsModels" ) == false &&
            stristr( $param, "totalRows_rsModels" ) == false ) {
            array_push( $newParams, $param );
        }
    }
    if ( count( $newParams ) != 0 ) {
        $queryString_rsModels = "&" . htmlentities( implode( "&", $newParams ) );
    }
}
$queryString_rsModels = sprintf( "&totalRows_rsModels=%d%s", $totalRows_rsModels, $queryString_rsModels );

$query_rsKeyInfo = sprintf( "SELECT apikey FROM apikeys WHERE service = %s AND active = 1", GetSQLValueString( "stt", "text" ) );
$rsKeyInfo = mysql_query( $query_rsKeyInfo, $transcribe )or die( mysql_error() );
$row_rsKeyInfo = mysql_fetch_assoc( $rsKeyInfo );


$apiKey = $row_rsKeyInfo['apikey'];

$curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://iam.bluemix.net/identity/token",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "grant_type=urn%3Aibm%3Aparams%3Aoauth%3Agrant-type%3Aapikey&apikey={$apiKey}",
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Postman-Token: 1d378144-7f93-4d72-8b2d-3d775883d3f3",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {

        $status = "cURL Error1 #:" . $err;
        

    } else {

        //echo "token response: {$response}<br>";

        $decodedData = json_decode($response);

        //var_dump($decodedData);

        $token = $decodedData->access_token;

        //echo "<p>token: {$token}</p>";
    }


?>
<style type="text/css">
    
#tipDiv {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    z-index: 100;
    background-color: rgba(0,0,0,0.5);
    display: none;
}
    .train {
        color: green;
    }
#closeTip {
    float: right;
    width: 100%;
    text-align: right;
    margin-bottom: 10px;
}
#tipContent p {
    margin-bottom: 20px;
}
#tipContent {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 400px;
    height: auto;
    margin-top: -150px;
    margin-left: -200px;
    background-color: white;
    padding: 20px;
    color: rgb(55, 55, 55);
}
</style>

<p>Custom language models allow you to expand the vocabulary of your transcription service for domain-specific terminology to improve transcription accuracy. To create custom corpora for your specific transcription needs, you must first create a model.<br><br>
<p><a href="create-model.php">Create new model</a></p><br>
<?php
if($totalRows_rsModels  > 0) {
?>

<table width="100%" cellpadding="5" cellspacing="5">
    <tbody>
        <tr>
            <td width="19%"><strong>Model Name</strong></td>
            <td width="22%"><strong>Language</strong></td>
            <td width="42%"><strong>Description</strong></td>
            <td width="17%"><strong>Status</strong></td>
        </tr>

        <?php do { 
    
            //$status = "Pending";
    
    
        if($row_rsModels['status'] != 3)
        {
            $status = "...";
            
            $curl2 = curl_init();

            curl_setopt_array($curl2, array(
              CURLOPT_URL => "https://stream.watsonplatform.net/speech-to-text/api/v1/customizations/{$row_rsModels['customizationid']}",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$token}",
                "cache-control: no-cache"
              ),
            ));

            $response2 = curl_exec($curl2);
            $err2 = curl_error($curl2);

            curl_close($curl2);

            if ($err2) {
              echo "cURL Error #:" . $err2;
            } else {
              
                //echo $response;
                
                $decodedData = json_decode($response2);

                $status = $decodedData->status;
                //$description = $decodedData->description;
                $description = $row_rsModels['modeldescription'];
                //$language = $decodedData->language;
                $language = str_replace("- Narrowband","",$row_rsModels['modelname2']);
                $cid = urlencode(en($row_rsModels['customizationid']));
                $mid = urlencode(en($row_rsModels['modelid']));
                
                //echo "id: {$id}";
                
                if ($status == "pending")
                {
                    //update table = 0
                    
                    $update = true;
                    $statusInd = 0;
                    
                    $status = "<a href='add-corpus.php?train=1&cid={$cid}'>$status</a><img src='img/tip.png' id='tipIcon0' style='height: 20px;
    width: 20px;'>";
                }
                else if ($status == "ready")
                {
                    //update table = 0
                    
                    $update = true;
                    $statusInd = 1;
                    
                    $status = "<a href='view-model.php?t=1&cid={$cid}&mid={$mid}' class='train'>$status</a><img src='img/tip.png' style='height: 20px;
    width: 20px;' id='tipIcon1'>";
                }
                else if ($status == "failed")
                {
                    //update table = 0
                    
                    $update = true;
                    $statusInd = 2;
                    
                    $status = "<a href='train.php?train=true&cid={$cid}'>train</a><img src='img/tip.png' style='height: 20px;
    width: 20px;' id='tipIcon2'>";
                }
                else if ($status == "available")
                {
                    $update = true;
                    $statusInd = 3;
                    
                    $code = urlencode(en($row_rsModels['code']));
        
                    $status = "<a href='transcribe.php?cid={$cid}&l={$code}'>available</a><img src='img/tip.png' style='height: 20px;
    width: 20px;' id='tipIcon3'>";
                }
                else
                {
                    //update table = 1
                }
                
                if($update == true)
                {
                    $updateSQL = sprintf("UPDATE custommodels SET status = %s WHERE customizationid = %s",
					GetSQLValueString(mysql_real_escape_string($statusInd), "text"),
					GetSQLValueString(mysql_real_escape_string($row_rsModels['customizationid']), "text"));
	
                    mysql_select_db($database_transcribe, $transcribe);
                    $Result1 = mysql_query($updateSQL, $transcribe) or die(mysql_error());
                    
                    //echo "update sql: {$updateSQL}";
                }
                
                
//                pending indicates that the model was created. It is waiting either for valid training data (corpora, grammars, or words) to be added or for the service to finish analyzing data that was added.
//ready indicates that the model contains valid data and is ready to be trained. If the model contains a mix of valid and invalid resources, training of the model fails unless you set the strict query parameter to false. For more information, see Training failures.
//training indicates that the model is being trained on data.
//available indicates that the model is trained and ready to use with a recognition request.
//upgrading indicates that the model is being upgraded.
//failed indicates that training of the model failed. Examine the words in the model's words resource to determine the errors that prevented the model from being trained.
            }
        }
    else
    {
        //$status = $decodedData->status;
                //$description = $decodedData->description;
                $description = $row_rsModels['modeldescription'];
                $language = str_replace("- Narrowband","",$row_rsModels['modelname2']);
                $id = urlencode(en($row_rsModels['customizationid']));
        //$status = "available";
        
        $code = urlencode(en($row_rsModels['code']));
        
        $status = "<a href='transcribe.php?cid={$id}&l={$code}'>available</a><img src='img/tip.png' style='height: 20px;
    width: 20px;' id='tipIcon3'>";
    }
        
        
        ?>
        <tr>
            <td class="hr"><a href="view-model.php?mid=<?php echo en($row_rsModels['modelid']); ?>"><?php echo $row_rsModels['modelname']; ?></a></td>
            <td><?php echo $language;?></td>
            <td><?php echo $description; ?></td>
            <td><?php echo $status; ?></td>
        </tr>

        <?php } while ($row_rsModels = mysql_fetch_assoc($rsModels)); ?>
    </tbody>
</table>



<p>
    <?php if ($pageNum_rsModels > 0) { // Show if not first page ?>
    <a href="<?php printf(" %s?pageNum_rsModels=%d%s ", $currentPage, 0, $queryString_rsModels); ?>">First</a>
    <?php } // Show if not first page ?>
    <?php if ($pageNum_rsModels > 0) { // Show if not first page ?>
    <a href="<?php printf(" %s?pageNum_rsModels=%d%s ", $currentPage, max(0, $pageNum_rsModels - 1), $queryString_rsModels); ?>">Previous</a>
    <?php } // Show if not first page ?>
    <?php if ($pageNum_rsModels < $totalPages_rsModels) { // Show if not last page ?>
    <a href="<?php printf(" %s?pageNum_rsModels=%d%s ", $currentPage, min($totalPages_rsModels, $pageNum_rsModels + 1), $queryString_rsModels); ?>">Next</a>
    <?php } // Show if not last page ?>
    <?php if ($pageNum_rsModels < $totalPages_rsModels) { // Show if not last page ?>
    <a href="<?php printf(" %s?pageNum_rsModels=%d%s ", $currentPage, $totalPages_rsModels, $queryString_rsModels); ?>">Last</a>
    <?php } // Show if not last page ?>
</p>

<?php } else { echo "You currently have no custom models."; }?>