<?php

$devStatus = "dev";

require_once( 'Connections/transcribe.php' );

include( "functions.php" );
include( "en-de.php" );

//test write

$colname_rsCorpora = "-1";
if ( isset( $_GET[ 'uid' ] ) ) {
    $colname_rsCorpora = $_GET[ 'uid' ];
}

if ( isset( $_SESSION[ 'uid' ] ) ) {
    $colname_rsCorpora = $_SESSION[ 'uid' ];
}

$currentPage = $_SERVER[ "PHP_SELF" ];

$maxRows_rsCorpora = 20;
$pageNum_rsCorpora = 0;
if ( isset( $_GET[ 'pageNum_rsCorpora' ] ) ) {
    $pageNum_rsCorpora = $_GET[ 'pageNum_rsCorpora' ];
}
$startRow_rsCorpora = $pageNum_rsCorpora * $maxRows_rsCorpora;

mysql_select_db( $database_transcribe, $transcribe );
$query_rsCorpora = sprintf( "SELECT a.*, b.code, b.modelid, b.modelname, c.modelname as 'modelname2'  FROM (SELECT * FROM corpus WHERE userid = {$colname_rsCorpora} ORDER by datecreated DESC) as a INNER JOIN (SELECT customizationid,code,modelname,modelid FROM custommodels) as b INNER JOIN (select modelname, code FROM modeloptions) as c ON a.customizationid = b.customizationid AND b.code = c.code" );

$query_limit_rsCorpora = sprintf( "%s LIMIT %d, %d", $query_rsCorpora, $startRow_rsCorpora, $maxRows_rsCorpora );
$rsCorpora = mysql_query( $query_limit_rsCorpora, $transcribe )or die( mysql_error() );
$row_rsCorpora = mysql_fetch_assoc( $rsCorpora );

if ( isset( $_GET[ 'totalRows_rsCorpora' ] ) ) {
    $totalRows_rsCorpora = $_GET[ 'totalRows_rsCorpora' ];
} else {
    $all_rsCorpora = mysql_query( $query_rsCorpora );
    $totalRows_rsCorpora = mysql_num_rows( $all_rsCorpora );
}
$totalPages_rsCorpora = ceil( $totalRows_rsCorpora / $maxRows_rsCorpora ) - 1;

$queryString_rsCorpora = "";
if ( !empty( $_SERVER[ 'QUERY_STRING' ] ) ) {
    $params = explode( "&", $_SERVER[ 'QUERY_STRING' ] );
    $newParams = array();
    foreach ( $params as $param ) {
        if ( stristr( $param, "pageNum_rsCorpora" ) == false &&
            stristr( $param, "totalRows_rsCorpora" ) == false ) {
            array_push( $newParams, $param );
        }
    }
    if ( count( $newParams ) != 0 ) {
        $queryString_rsCorpora = "&" . htmlentities( implode( "&", $newParams ) );
    }
}
$queryString_rsCorpora = sprintf( "&totalRows_rsCorpora=%d%s", $totalRows_rsCorpora, $queryString_rsCorpora );

$query_rsKeyInfo = sprintf( "SELECT apikey FROM apikeys WHERE service = %s AND active = 1", GetSQLValueString( "stt", "text" ) );
$rsKeyInfo = mysql_query( $query_rsKeyInfo, $transcribe )or die( mysql_error() );
$row_rsKeyInfo = mysql_fetch_assoc( $rsKeyInfo );

$apiKey = $row_rsKeyInfo[ 'apikey' ];

$curl = curl_init();

curl_setopt_array( $curl, array(
    CURLOPT_URL => "https://iam.bluemix.net/identity/token",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "grant_type=urn%3Aibm%3Aparams%3Aoauth%3Agrant-type%3Aapikey&apikey={$apiKey}",
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Postman-Token: 1d378144-7f93-4d72-8b2d-3d775883d3f3",
        "cache-control: no-cache"
    ),
) );

$response = curl_exec( $curl );
$err = curl_error( $curl );

curl_close( $curl );

if ( $err ) {

    $status = "cURL Error1 #:" . $err;


} else {

    //echo "token response: {$response}<br>";

    $decodedData = json_decode( $response );

    //var_dump($decodedData);

    $token = $decodedData->access_token;

    //echo "<p>token: {$token}</p>";
}


?>

<p style="margin-bottom: 15px">Creating a corpus allows you to expand the vocabulary of your transcription service for domain-specific terminology. For industries such as medicine, government, and sports.</p>
<p>For more information on corpora, see <a href="corpora-details.php">Corpora details.</a>
</p>
<br>
<p><a href="add-corpus.php">Add corpus</a>
</p>
<br>
<?php

if ( $totalRows_rsCorpora > 0 ) {
    ?>

<table width="100%" cellpadding="5" cellspacing="5">
    <tbody>
        <tr>
            <td width="19%"><strong>Filename</strong>
            </td>
            <td width="42%"><strong>Content</strong>
            </td>
            <td width="22%"><strong>Model</strong>
            </td>
            <td width="17%"><strong>Status</strong>
            </td>
        </tr>

        <?php do { 
        
        
        if($row_rsCorpora['status'] != 1)
        {
            $status = "...";
            
            $curl2 = curl_init();
            
            curl_setopt_array($curl2, array(
              CURLOPT_URL => "https://stream.watsonplatform.net/speech-to-text/api/v1/customizations/{$row_rsCorpora['customizationid']}/corpora",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$token}",
                "cache-control: no-cache"
              ),
            ));

            $response2 = curl_exec($curl2);
            $err2 = curl_error($curl2);

            curl_close($curl2);

            if ($err2) {
              echo "cURL Error #:" . $err2;
            } else {
              
                //echo $response;
                
                $decodedData = json_decode($response2);

                $status = $decodedData->corpora[0]->status;
                
                if($status == "being_processed")
                {
                    $status = "Being Processed";
                }
                else if($status == "undetermined")
                {
                    $status = "Undetermined";
                }
                else
                {
                    $status = "Analyzed";
                }
                
                //echo "id: {$id}";
                
//                if ($status == "pending")
//                {
//                    //update table = 0
//                    
//                    $status = "<a href=\"add-corpus.php?train=true&id={$id}\">$status</a><img src=\"img/tip.png\" id=\"tipIcon\" style=\"height: 20px;
//    width: 20px;\">";
//                }
//                else if ($status == "ready")
//                {
//                    //update table = 0
//                    
//                    $status = "<a href='train.php?train=true&id={$id}\">train</a><img src='img/tip.png' style='height: 20px;
//    width: 20px;' id='tipIcon'>";
//                }
//                else
//                {
//                    //update table = 1
//                }
            }
        }
    else
    {
        $status = "Analyzed";
    }
        
        
        ?>
        <tr>
            <td>
                <a href="edit-corpus.php?cpid=<?php echo urlencode(en($row_rsCorpora['corpusid'])); ?>&cid=<?php echo urlencode(en($row_rsCorpora['customizationid'])); ?>">
                    <?php echo $row_rsCorpora['filename']; ?>.txt</a>
            </td>
            <td>
                <?php echo substr($row_rsCorpora['content'],0,30)."..."?>
            </td>
            <?php /*?>
            <td>
                <?php echo blankNull( str_replace(" - Narrowband","",$row_rsCorpora['modelname2'])); ?>
            </td>
            <?php */?>
            <td>
                <a href="view-model.php?mid=<?php echo urlencode(en($row_rsCorpora['modelid'])); ?>&cid=<?php echo urlencode(en($row_rsCorpora['customizationid'])); ?>">
                    <?php echo $row_rsCorpora['modelname']; ?>
                </a>
            </td>
            <td>Analyzed</td>
        </tr>

        <?php } while ($row_rsCorpora = mysql_fetch_assoc($rsCorpora)); ?>
    </tbody>
</table>

<p>&nbsp;
    <?php if ($pageNum_rsCorpora > 0) { // Show if not first page ?>
    <a href="<?php printf(" %s?pageNum_rsCorpora=%d%s ", $currentPage, 0, $queryString_rsCorpora); ?>">First</a>
    <?php } // Show if not first page ?>
    <?php if ($pageNum_rsCorpora > 0) { // Show if not first page ?>
    <a href="<?php printf(" %s?pageNum_rsCorpora=%d%s ", $currentPage, max(0, $pageNum_rsCorpora - 1), $queryString_rsCorpora); ?>">Previous</a>
    <?php } // Show if not first page ?>
    <?php if ($pageNum_rsCorpora < $totalPages_rsCorpora) { // Show if not last page ?>
    <a href="<?php printf(" %s?pageNum_rsCorpora=%d%s ", $currentPage, min($totalPages_rsCorpora, $pageNum_rsCorpora + 1), $queryString_rsCorpora); ?>">Next</a>
    <?php } // Show if not last page ?>
    <?php if ($pageNum_rsCorpora < $totalPages_rsCorpora) { // Show if not last page ?>
    <a href="<?php printf(" %s?pageNum_rsCorpora=%d%s ", $currentPage, $totalPages_rsCorpora, $queryString_rsCorpora); ?>">Last</a>
    <?php } // Show if not last page ?>
</p>

<?php } else { echo "You currently have no saved corpora."; }?>