<?php

$curl2 = curl_init();

$data = "{\"text\": [\"{$input}\"], \"model_id\":\"{$lang}\"}";

//echo "<p>{$data}</p>";

curl_setopt_array( $curl2, array(
    CURLOPT_URL => "https://gateway.watsonplatform.net/language-translator/api/v3/translate?version=2018-05-01",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $data,
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer {$token}",
        "Content-Type: application/json",
        "Postman-Token: ca931d40-42a1-49ca-b711-70fe511252fd",
        "cache-control: no-cache"
    ),
) );


$response2 = curl_exec( $curl2 );
$err2 = curl_error( $curl2 );

curl_close( $curl2 );

if ( $err2 ) {

    echo "<p>cURL Error2 #:" . $err2 . "</p>";

} else {

    //echo "<p>convert response: {$response2}</p>";

    //echo "conversion successful";

    $endtime = date( "Y-m-d H:i:s" );

    $timeAdded = blankNull( humanTiming( strtotime( $starttime ) ) ) . " ago";

    //echo ". before parse";
    //parse result and save in db

    include( "parseTranslation.php" );

    //send email

    //echo ". after parse";

    $message = "Document translated successfully. Total Time: {$timeAdded}";

    $to = "registerrt1224@gmail.com";
    $subject = "Transcribe completed successfully";
    $html = $message;
    $text = $message;
    $from = "noreply@transcribe.com";

    //include("../send-email.php");               
}

?>