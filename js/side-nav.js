// JavaScript Document 2

var navShown = false;
var width;

$(document).ready(function () {

    var pageURL = $(location).attr("href");

    if (pageURL.indexOf("translate.php") >= 0 || pageURL.indexOf("transcribe.php") >= 0) {
        width = 800;
    } else {
        width = 600;
    }

    //console.log("pageURL: " + pageURL);

    $('#navIcon').on('click', function () {

        //alert("get file");

        if (navShown) {
            //console.log("close");
            closeNav();
            navShown = false;
        } else {
            //console.log("open");
            openNav();
            navShown = true;
        }

    });

    $(window).resize(function () {

        //console.log("resize");

        if ($(window).width() > width) {
            closeNav();
        }
    });
})


function openNav() {

    //console.log("open");

    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("primaryContainer").style.marginLeft = "250px";
}

function closeNav() {

    //console.log("close");

    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("primaryContainer").style.marginLeft = "0";
}
