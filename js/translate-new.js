// JavaScript Document

var language = "";
var fileName = "";
var hasFile = false;
var hasModel = false;
var estimatedCost;

$(document).ready(function () {

    //console.log("ready3");

    //disable button

    $("#sourceLanguage").on('change', function () {

        if ($("#sourceLanguage").val() != "Choose output language...") {
            
            $("#sourceLanguage").css("background-color", "rgba(248,248,248,1.0)");
            
            hasModel = true;
            
            if(hasFile && hasModel)
            {
                $("#transcribeBtn").css("background-color", "rgb(155, 197, 61)");
            }
            
        } else {
            
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
            $("#transcribeBtn").css("background-color", "gray");
            hasModel = false;
        }
    })

    $("#transcribeBtn").css("background-color", "gray");

    $('#transcribeBtn').on('click', function () {

        //alert("get file");

        if (!hasFile) {
            
            $("#languageTxt").html("Please select a file.");
            return;
        }

        if ($("#sourceLanguage").val() == "Choose output language...") {
            
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
            return;
        }
        
        if(hasModel && hasFile)
        {
            translate();
        }

    });

    //$("#transcribeBtn").attr("disabled", 'disabled');

    $("#SELECT_FILE").on('change', function () {

        var file = $('#SELECT_FILE').prop('files')[0];
        var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();

        if ($.inArray(ext, ['txt', 'doc', 'docx', 'pdf']) == -1) {

            //$("#fileNameTxt").html("Invalid file type");
            $("#transcribeBtn").css("background-color", "gray");
            $('#languageTxt').html("Invalid file type. Only .txt, .doc, .docx and .pdf files are accepted.");
            //rgb(155, 197, 61);
            hasFile = false;

        } else {

            //$('#transcribeBtn').prop('disabled', false);
            hasFile = true;
            
            if(hasFile && hasModel)
            {
                $("#transcribeBtn").css("background-color", "rgb(155,197,61)");
            }            
            
            readURL(this);
            getWordCount(file);

        }
    });

    $("#selectFileBtn").on('click', function () {

        //console.log("click div");
        $("#SELECT_FILE").click();
    });
})

function readURL (input) {
    
    //console.log("read");

    if (input.files && input.files[0]) {
        
        //console.log("read2");

        var reader = new FileReader();

        reader.onload = function (e) {

            fileName = input.files[0].name;

            //console.log("file: " + input.files[0].name);
            
            $("#fileNameTxt").html(fileName);
            $('#languageTxt').html("");

            //$('.profile-pic').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function getWordCount(file) {

    var file_data = file;
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: 'getWordCount.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //readURL(file)

            //console.log(php_script_response);

            estimatedCost = parseFloat(php_script_response);

            //console.log("estimatedCost : " + estimatedCost);

            var str = file["name"] + "<br>Cost: $" + estimatedCost.toFixed(2);

            $('#fileNameTxt').html(str);
        }
    });
}


function translate() {

    var file = $('#SELECT_FILE').prop('files')[0];

    if (file == null) {
        $('#languageTxt').html("Please select a file");
        return;
    }

    var uid = $('#uid').val();
    language = $('#sourceLanguage').val();
    var displayLanguage = $("#sourceLanguage option:selected").text();
    
    var fileType = file["type"];
    var validImageTypes = ["text/txt", "text/pdf", "text/doc", "text/docx"];
    var fileExtensions = ['txt','pdf','doc','docx'];
    var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();
    //var pre = $('#fileField').val().split('.')[0].toLowerCase();

    //console.log("ext: " + ext);
    //console.log("client fileType: " + fileType);

    var form_data = new FormData();

    if (language == "Choose output language...") {

        $('#languageTxt').html("Please select language");

        return;
    } else {
        $('#languageTxt').html("");
    }

    if ($.inArray(fileType, validImageTypes) < 0 && ($.inArray(ext, fileExtensions) < 0)) {

        // invalid file type code goes here.

        $('#languageTxt').html("Invalid file type. Only .txt, .doc, .docx and .pdf files are accepted.");

    } else {

        form_data.append('file', file);
        form_data.append('language', language);
        form_data.append('displayLanguage', displayLanguage);
        form_data.append('fileType', fileType);
        form_data.append('fileName', fileName);
        form_data.append('estimatedCost', estimatedCost);
        

        //console.log("uid: " + uid + " language: " + language + " displayLanguage: " + displayLanguage + " fileType: " + fileType + " fileName: " + fileName + " estimatedCost: " + estimatedCost);
        
        //return;

        form_data.append('uid', uid);
        //form_data.append('did', did);
        //form_data.append('input', input);

        $('#SELECT_FILE').val('');

        $.ajax({
            url: 'IBM-translate.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log(php_script_response);
                
                var obj = jQuery.parseJSON(php_script_response);
                var translationid = obj.translationid;

                if (translationid != null) {
                    
                    window.location.href = "edit-translation.php?tid=" + translationid + "&language=" + language;
                }
            }
        });
    }
}
