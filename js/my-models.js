// JavaScript Document

var lang;
var timer;
var done;
var did;
var filename;

$(document).ready(function () {

    //console.log("go");

    getModelData();
})

function closeTip() {

    $("#tipDiv").css("display", "none");
}

function openTip(status1) {

    //console.log("open tip " + status1);

    if (status1 == "pending") {
        $("#tipTxt").html("<strong>Pending</strong> indicates that the model was created. It is waiting for valid training data from a corpora to be added to finish analyzing data.");
    } else if (status1 == "ready") {
        $("#tipTxt").html("<strong>Ready</strong> indicates that the model contains valid data and is ready to be trained.");
    } else if (status1 == "failed") {
        $("#tipTxt").html("<strong>Failed</strong> indicates that training of the model failed. Examine the words in the model's words resource to determine the errors that prevented the model from being trained.");
    } else if (status1 == "available") {
        $("#tipTxt").html("<strong>Available</strong> indicates that the model is trained and ready to use with a recognition request.");
    }

    $("#tipDiv").css("display", "block");
}

function getModelData() {

    //console.log("getModelData");

    var pageNum_rsFiles = getUrlParameter('pageNum_rsFiles');
    var totalRows_rsFiles = getUrlParameter('totalRows_rsFiles');

    var form_data = new FormData();
    form_data.append('pageNum_rsFiles', pageNum_rsFiles);
    form_data.append('totalRows_rsFiles', totalRows_rsFiles);

    $.ajax({
        url: 'getModelsHTML.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("docData: " + php_script_response);

            $('#renderContent').html(php_script_response);

            $('#closeTip').on('click', function () {

                closeTip();
            });

            $('#tipIcon0').on('click', function () {

                openTip("pending");
            });

            $('.tipIcon1').on('click', function () {

                openTip("ready");
            });

            $('.tipIcon2').on('click', function () {

                openTip("failed");
            });

            $('#tipIcon3').on('click', function () {

                openTip("available");
            });
            
            $('.tipIcon3').on('click', function () {

                openTip("available");
            });


        }
    });
}
