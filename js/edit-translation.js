// JavaScript Document


$(document).ready(function () {

    //console.log("ready2");

    $("#docContentDiv").val();

    $("#exportBG").css("display", "none");
    $("#resultLbl").css("display", "none");
    $("#paymentDiv").css("display", "none");

    $('#cancel_button').on('click', function () {

        $("#docContentDiv").css("display", "block");
        $("#editTxt").css("display", "none");
        $("#cancel_button").css("display", "none");
        $("#save_button").css("display", "none");
        $("#edit_button").css("display", "block");
    });

    $('#edit_button').on('click', function () {

        $("#docContentDiv").css("display", "none");
        $("#editTxt").css("display", "block");
        $("#cancel_button").css("display", "block");
        $("#save_button").css("display", "block");
        $("#edit_button").css("display", "none");
    });

    $('#save_button').on('click', function () {

        //alert("get file");

        updateTranslation();
    });

    getData();
})

function getData() {

    //console.log("getData");

    var tid = $('#tid').val();
    var did = $('#did').val();
    var uid = $('#uid').val();
    var lang = $('#l').val();

    //console.log("uid: " + uid + " tid: " + tid + " lang: " + lang);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('tid', tid);
    form_data.append('lang', lang);

    $.ajax({
        url: 'docData.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("docData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var translation = obj.translation;
            var displayLang = obj.displayLang;
            var filename = obj.filename;

            $('#headerLbl').html("Edit Translation: " + filename);
            $('#langLbl').html("Output Language: " + displayLang);
            
            var str = translation + "<br><br><a href='document-translations.php?did="+encodeURIComponent(did)+"'>Back</a>";

            $('#docContentDiv').html(str);
            $('#editTxt').val(translation);
        }
    });
}

function updateTranslation() {

    //console.log("updateTranslation");

    var tid = $('#tid').val();
    var uid = $('#uid').val();
    var input = $('#editTxt').val();
    var did = $('#did').val();
    //var lang = $('#l').val();

    //console.log("uid: " + uid + "\ntid: " + tid + "\ninput: " + input);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('tid', tid);
    form_data.append('input', input);
    
    //return;

    $.ajax({
        url: 'update-translation.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("translatonData: " + php_script_response);

            if (php_script_response == "translation updated successfully") {
                
                //console.log("docData: " + php_script_response);
                        
                var str = $('#editTxt').val() + "<br><br><a href='document-translations.php?did="+ encodeURIComponent(did)+"'>Back</a>";

                $('#docContentDiv').html(str);
                
                //$('#docContentDiv').html($('#editTxt').val());
                //$('#editTxt').val(php_script_response);

                $("#docContentDiv").css("display", "block");
                $("#editTxt").css("display", "none");

                $("#cancel_button").css("display", "none");
                $("#save_button").css("display", "none");
                $("#edit_button").css("display", "block");
            }
        }
    });
}
