// JavaScript Document

var lang;
var timer;
var done;
var did;
var filename;

$(document).ready(function () {

    getCorpusData();
})

function getCorpusData() {

    //console.log("getCorpusData");
    
    var pageNum_rsFiles = getUrlParameter('pageNum_rsFiles');
    var totalRows_rsFiles = getUrlParameter('totalRows_rsFiles');    
    
    var form_data = new FormData();
    form_data.append('pageNum_rsFiles', pageNum_rsFiles);
    form_data.append('totalRows_rsFiles', totalRows_rsFiles);

    $.ajax({
        url: 'getCorpusHTML.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("docData: " + php_script_response);

            $('#renderContent').html(php_script_response);
        }
    });
}

