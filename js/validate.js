// JavaScript Document

$(document).ready(function () {

    $("#exp")
        .focusout(function () {
        
            var value = $("#exp").val();

            console.log(value);

            if (isValidMonth(value)) {
                console.log("valid");
            } else {
                $("#exp").val("");
            }
        });
    
    $("#exp2")
        .focusout(function () {
        
            var value = $("#exp2").val();

            console.log(value);

            if (isValidYear(value)) {
                console.log("valid");
            } else {
                $("#exp2").val("");
            }
        });
})

function isValidMonth(dateString) {
    var regEx = /^\d{2}$/;
    if (!dateString.match(regEx)) {return false} else {return true}; // Invalid format
}

function isValidYear(dateString) {
    var regEx = /^\d{4}$/;
    if (!dateString.match(regEx)) {return false} else {return true}; // Invalid format
}
