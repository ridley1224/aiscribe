// JavaScript Document

var lang;

$(document).ready(function () {

    //console.log("ready translate");

    $("#docContentDiv").val();

    $("#exportBG").css("display", "none");
    $("#resultLbl").css("display", "none");
    $("#paymentDiv").css("display", "none");
    $("#edit_button").css("display", "none");
    $("#resultLangLbl").css("display", "none");

    $('#translate_button').on('click', function () {

        if ($("#sourceLanguage").val() != "Choose output language...") {
            translate();
        } else {
            //console.log("disable");
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
        }
    });

    $("#sourceLanguage").on('change', function () {

        if ($("#sourceLanguage").val() != "Choose output language...") {
            $("#sourceLanguage").css("background-color", "rgba(248,248,248,1.0)");
        } else {
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
        }
    })

    $('#cancel_button').on('click', function () {

        $("#docContentDiv").css("display", "block");
        $("#editTxt").css("display", "none");
        $("#cancel_button").css("display", "none");
        $("#save_button").css("display", "none");
        $("#edit_button").css("display", "block");
    });

    $('#edit_button').on('click', function () {

        editDoc();
    });

    getData();
})

function editDoc() {

    var did = $('#did').val();
    //var lang = $('#sourceLanguage').val();

    window.location.href = "edit-translation.php?did=" + did + "&lang=" + lang;
}

function paymentDone() {

    //console.log("paymentDone");

    $("#docContentDiv").css("display", "none");
    $("#editTxt").css("display", "block");
    $("#translate_button").css("display", "none");
    $("#cancel_button").css("display", "block");
    $("#save_button").css("display", "block");
    //$("#edit_button").css("display", "block");
}

function getData() {

    //console.log("getData");

    var did = $('#did').val();
    var uid = $('#uid').val();

    //console.log("uid: "+uid+" did: "+did);

    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('did', did);

    $.ajax({
        url: 'docData.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("docData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);
            var response = obj.response.stripSlashes();

            $('#docContentDiv').html(response);
            $('#editTxt').val(response);
        }
    });
}

function getWordCount(file) {

    var file_data = file;
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: 'getWordCount.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log(php_script_response);

            var estimatedCost = parseFloat(php_script_response);

            //console.log("estimatedCost : " + estimatedCost);

            var str = file["name"] + "<br>Cost: $" + estimatedCost.toFixed(2);

            $('#fileNameTxt').html(str);
        }
    });
}

function translate() {

    //console.log("translate");

    var did = $('#did').val();
    var uid = $('#uid').val();
    var input = $('#editTxt').val();
    lang = $('#sourceLanguage').val();
    var langDisplay = $("#sourceLanguage option:selected").text();

    //console.log("lang: " + lang);
    //console.log("langDisplay: " + langDisplay);

    if (lang != "Choose output language...") {

        //console.log("uid: " + uid + " did: " + did + " lang: " + lang);
        //console.log("input: " + input);

        //var file_data = $('#SELECT_FILE').prop('files')[0];   
        var form_data = new FormData();
        form_data.append('uid', uid);
        //form_data.append('did', did);
        form_data.append('input', input);
        form_data.append('language', lang);
        form_data.append('langDisplay', langDisplay);

        $.ajax({
            url: 'IBM-translate.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log("translationData: " + php_script_response);

                var obj = jQuery.parseJSON(php_script_response);

                var response = obj.translation;
                var translationid = obj.translationid;

                $('#docContentDiv').html(response);
                $('#editTxt').val(response);
                $("#edit_button").css("display", "block");
                $("#translate_button").css("display", "none");
                $("#sourceLbl").html("Output: " + langDisplay);
                $("#sourceLanguage").css("display", "none");
            }
        });
    }
}

function upload() {

    var file = $('#SELECT_FILE').prop('files')[0];
    var fileType = file["type"];
    var validImageTypes = ["audio/mp3", "audio/mpeg", "audio/wav", "audio/x-wav", "audio/ogg", "audio/flac"];
    var fileExtensions = ['ogg'];
    var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();
    //var pre = $('#fileField').val().split('.')[0].toLowerCase();

    //console.log("ext: " + ext);

    //console.log("client fileType: " + fileType);

    if ($.inArray(fileType, validImageTypes) < 0 && ($.inArray(ext, fileExtensions) < 0)) {

        // invalid file type code goes here.

        $('#uploadStatus').html("Invalid file type. Only audio formats .mp3, .wav, .ogg and .flac files are accepted.");

    } else {

        var form_data = new FormData();
        form_data.append('file', file);

        //$('#uploadStatus').html("File sent");
        $('#uploadStatus').html(file["name"] + ": transcription in progress");
        $('#SELECT_FILE').val('');

        //alert(form_data);     

        $.ajax({
            url: 'upload.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log(php_script_response);

                if (php_script_response == "file uploaded successfully. conversion successful" || php_script_response == "file uploaded successfully. ") {

                    $('#uploadStatus').html(file["name"] + ": Complete");
                }
            }
        });
    }
}
