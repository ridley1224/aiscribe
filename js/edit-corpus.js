// JavaScript Document
var filename;
var cid;
var cpid;
var timer;
var timerStarted;

$(document).ready(function () {

    //inactivityTime();

    $("#docContentDiv").val();

    $("#exportBG").css("display", "none");
    $("#resultLbl").css("display", "none");
    $("#paymentDiv").css("display", "none");

    $('#cancel_button').on('click', function () {

        $("#docContentDiv").css("display", "block");
        $("#editTxt").css("display", "none");
        $("#cancel_button").css("display", "none");
        $("#save_button").css("display", "none");
        $("#edit_button").css("display", "block");
    });

    $('#edit_button').on('click', function () {

        $("#docContentDiv").css("display", "none");
        $("#editTxt").css("display", "block");
        $("#cancel_button").css("display", "block");
        $("#save_button").css("display", "block");
        $("#edit_button").css("display", "none");
    });

    $('#save_button').on('click', function () {

        //alert("get file");

        updateCorpus();
    });

    getData();

    cid = getUrlParameter('cid');
    cpid = getUrlParameter('cpid');

    if (cid != null && cpid != null) {

        refreshCorpusStatus();
    }
})

var inactivityTime = function () {

    window.onload = resetTimer;
    // DOM Events
    document.onmousemove = resetTimer;
    document.onkeypress = resetTimer;
};

function stopRefresh() {

    done = true;
    timerStarted = false;
    clearTimeout(timer);
}

function resetTimer() {

    clearTimeout(time);

    if (!done) {
        time = setTimeout(stopRefresh, 60000 * 5);
    }

    // 1000 milliseconds = 1 second
}

function closeTip() {

    $("#tipDiv").css("display", "none");
}

function openTip(status1) {

    //console.log("open tip " + status1);

    if (status1 == "being_processed") {
        $("#tipTxt").html("<strong>Being Processed</strong> indicates that the service is still analyzing the corpus. The service cannot accept requests to add new corpora or words, or to train the custom model, until its analysis is complete.");
    } else if (status1 == "failed") {
        $("#tipTxt").html("<strong>Undetermined</strong> indicates that the service encountered an error while processing the corpus. The information that is returned for the corpus includes an error message that offers guidance for correcting the error.");
    } else {
        $("#tipTxt").html("<strong>Analyzed</strong> indicates that the service successfully analyzed the corpus. The custom model can be trained with data from the corpus.");
    }

    $("#tipDiv").css("display", "block");
}

function getData() {

    //console.log("getData");

    var cpid = $('#cpid').val();
    var uid = $('#uid').val();

    //console.log("uid: " + uid + " cpid: " + cpid);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('cpid', cpid);

    $.ajax({
        url: 'docData.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("docData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var content = obj.content;
            filename = obj.filename;

            $('#headerLbl').html("Edit Corpus: " + filename);
            //$('#langLbl').html("Output Language: " + displayLang);

            var str = content + "<br><br><a href='my-corpora.php'>Back</a>";

            $('#docContentDiv').html(str);
            $('#editTxt').val(content);
        }
    });
}

function updateCorpus() {

    //console.log("updateCorpus");

    var cpid = $('#cpid').val();
    var cid = $('#cid').val();
    var uid = $('#uid').val();
    var content = $('#editTxt').val();
    //var filename = $('#filename').val();
    //var lang = $('#lang').val();

    //console.log("uid: " + uid + "\ncid: " + cid);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('cid', cid);
    form_data.append('cpid', cpid);
    form_data.append('content', content);

    form_data.append('filename', filename);

    //    //console.log("filename: " + filename);
    //    //console.log("cpid: " + cpid);

    //return;

    //console.log("go");

    //form_data.append('did', did);
    //form_data.append('input', input);

    //$('#SELECT_FILE').val('');

    $.ajax({
        url: 'IBM-create-corpus.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log(php_script_response);

            var obj = jQuery.parseJSON(php_script_response);
            var status1 = obj.status;
            var cid = obj.cid;
            var mid = obj.mid;

            if (status1.indexOf("corpus created successfully") >= 0 || status1.indexOf("corpus updated successfully") >= 0) {

                window.location.href = "view-model.php?mid=" + mid + "&cid=" + cid;
            }
        }
    });
}

function refreshCorpusStatus() {

    //console.log("refreshCorpusStatus");

    var form_data = new FormData();
    form_data.append('cid', cid);

    //console.log("customizationid " + cid);

    $.ajax({
        url: 'IBM-corpus-info.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("response: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);
            var status = obj.status;

            var statusHTML = "";

            if (status == "being_processed") {
                statusHTML = status + "<img src='img/tip.png' id='tipIcon0' class='tipIcon0' style='height: 20px; width: 20px;cursor: pointer;'>";
            } else if (status == "undetermined") {
                statusHTML = status + "<img src='img/tip.png' id='tipIcon1' class='tipIcon1' style='height: 20px; width: 20px;cursor: pointer;'>";
            } else {
                statusHTML = status + "<img src='img/tip.png' id='tipIcon2' class='tipIcon2' style='height: 20px; width: 20px;cursor: pointer;'>";
            }

            $('#statusDiv').html(statusHTML);

            $('#closeTip').on('click', function () {

                closeTip();
            });

            $('.tipIcon0').on('click', function () {

                openTip("being_processed");
            });

            $('.tipIcon1').on('click', function () {

                openTip("undetermined");
            });

            $('.tipIcon2').on('click', function () {

                openTip("analyzed");
            });

            if (status == "being_processed") {

                //console.log("start timer");

                if (done == false) {

                    timerStarted = true;
                    inactivityTime();
                    timer = setTimeout(runTimer, 10000);
                }
            } else {
                //console.log("stop timer");
                done = true;
                timerStarted = false;
                clearTimeout(timer);
            }
        }
    });
}

function runTimer() {

    //console.log("run it");
    refreshCorpusStatus();
}
