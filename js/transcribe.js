// JavaScript Document 2

var hasFile = false;
var estimatedCost;
var hasModel = false;
var cid;
var language;

$(document).ready(function () {

    //console.log("ready transcribe");

    $("#transcribeBtn").css("background-color", "gray");

    $('#closeTip').on('click', function () {

        closeTip();
    });

    $('#tipIcon').on('click', function () {

        openTip();
    });

    $("#sourceLanguage").on('change', function () {

        if ($("#sourceLanguage").val() != "Choose audio language...") {
            $("#sourceLanguage").css("background-color", "rgba(248,248,248,1.0)");

            hasModel = true;

            if (hasFile) {
                $("#transcribeBtn").css("background-color", "rgb(155, 197, 61)");
            }
        } else {
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
            $("#transcribeBtn").css("background-color", "gray");
            hasModel = false;
        }

        getLanguageModels($("#sourceLanguage").val());
    })

    cid = getUrlParameter('cid');
    language = getUrlParameter('l');

    if (language) {
        //set selected
        //console.log("language set");
        getLanguageModels($("#sourceLanguage").val());
        //getLanguageModels(language);
    }


    $('#transcribeBtn').on('click', function () {

        //alert("get file");

        //        if (hasFile) {
        //
        //            transcribe();
        //        }

        if (!hasFile) {
            $("#fileNameTxt").html("Please select a file.");
            return;
        }

        if ($("#sourceLanguage").val() == "Choose audio language...") {
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
            return;
        }

        if (hasModel && hasFile) {
            transcribe();
        }
    });

    $("#SELECT_FILE").on('change', function () {

        var file = $('#SELECT_FILE').prop('files')[0];

        var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();

        if ($.inArray(ext, ['wav', 'mp3', 'ogg', 'flac']) == -1) {

            $("#transcribeBtn").css("background-color", "gray");
            $("#fileNameTxt").html("Invalid file type. Only audio formats .mp3, .wav, .ogg and .flac files are accepted.");
            hasFile = false;

        } else {

            checkAudio(file);
        }
    });


    $("#selectFileBtn").on('click', function () {

        //console.log("click div");

        $("#SELECT_FILE").click();
    });
})

function closeTip() {

    $("#tipDiv").css("display", "none");
}

function openTip() {

    $("#tipDiv").css("display", "block");
}

var readURL = function (input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            //console.log("file: " + input.files[0].name);
            $("#fileNameTxt").html(input.files[0].name);

            //$('.profile-pic').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function getLanguageModels(language) {

    var uid = $('#uid').val();

    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('code', language);


    if (cid) {
        var tempid = $('#tempid').val();
        //console.log("sending ID: " + cid);
        form_data.append('cid', cid);
    }

    $.ajax({
        url: 'getLanguageModels.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            $('#customDiv').html(php_script_response);
        }
    });
}

function checkAudio(file) {

    var file_data = file;
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: 'checkAudio.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log(php_script_response);

            var audioLength = php_script_response;

            //console.log("audioLength: " + audioLength);

            if (audioLength > 0) {

                $("#transcribeBtn").css("background-color", "rgb(155,197,61)");
                readURL(file);
                hasFile = true;

                var costPerSecond = 0.02 / 60;
                var markup = 0.5;
                var markupTotal = (costPerSecond * markup) + costPerSecond;

                //console.log("costPerSecond: " + costPerSecond);
                //console.log("markupTotal: " + markupTotal);

                estimatedCost = audioLength * markupTotal;

                //console.log("estimatedCost: " + estimatedCost.toFixed(2));

                if (estimatedCost < 1) {
                    estimatedCost = 1;
                }

                //console.log("estimatedCost after adjustment: " + estimatedCost.toFixed(2));

                var str = file["name"] + "<br>Estimated Cost: $" + estimatedCost.toFixed(2);

                $('#fileNameTxt').html(str);
            } else {
                $('#fileNameTxt').html("Audio files must be at least 30 seconds in length.");
                $("#transcribeBtn").css("background-color", "gray");
                hasFile = false;
            }
        }
    });
}

function checkInternet() {

    if (0 == 0) {
        transcribe();
    }
}

function transcribe() {

    var file = $('#SELECT_FILE').prop('files')[0];

    if (file == null) {

        $('#fileNameTxt').html("Please select a file");
        return;
    }

    var fileType = file["type"];
    var fileName = file["name"];
    var validImageTypes = ["audio/mp3", "audio/mpeg", "audio/wav", "audio/x-wav", "audio/ogg", "audio/flac"];
    var fileExtensions = ['ogg'];

    var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();
    //var pre = $('#fileField').val().split('.')[0].toLowerCase();

    var uid = $('#uid').val();
    var language = $('#sourceLanguage').val();
    var customizationid = $('#customModel').val();

    //console.log("ext: " + ext);
    //console.log("client fileType: " + fileType);
    //console.log("language: " + language);
    //console.log("customizationid: " + customizationid);

    //return;

    if ($.inArray(fileType, validImageTypes) < 0 && ($.inArray(ext, fileExtensions) < 0)) {

        // invalid file type code goes here.

        $('#fileNameTxt').html("Invalid file type. Only audio formats .mp3, .wav, .ogg and .flac files are accepted.");

    } else {

        var file_data = $('#SELECT_FILE').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('uid', uid);
        form_data.append('language', language);
        form_data.append('estimatedCost', estimatedCost);

        if (customizationid != "Custom models..." && customizationid != "undefined") {
            form_data.append('customizationid', customizationid);
        }

        //$('#uploadStatus').html("File sent");
        $('#fileNameTxt').html(file["name"] + ": transcription in progress");
        $('#SELECT_FILE').val('');

        //alert(form_data);     

        //show timer?

        $.ajax({
            url: 'initTranscribe.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log(php_script_response);

                var obj = jQuery.parseJSON(php_script_response);
                var did = obj.did;
                var status = obj.status;

                if (status == "document saved") {

                    window.location.href = "my-files.php?did=" + did + "&filename=" + fileName;
                } else {
                    $('#fileNameTxt').html(status);
                }
            }
        });
    }
}

//https://stackoverflow.com/questions/29805909/jquery-how-to-check-if-uploaded-file-is-an-image-without-checking-extensions
