// JavaScript Document

var fileName = "";
var hasFile = false;
var displayLanguages;
var displayLanguage;
var languages;
var language;
var ind = 0;
var input;
var did;
var credits;
var amount;
var remainingCredits;
var uid;

$(document).ready(function () {

    did = $('#did').val();
    uid = $('#uid').val();
    languages = $("#languages").val().split(",");
    displayLanguages = $("#displayLanguages").val().split(",");

    //console.log("languages: " + languages);
    //console.log("displayLanguages: " + displayLanguages);
    
    credits = $('#credits').val();
    amount = $('#subtotal').val();
    
    //console.log("credits: " + credits + " amount: " + amount);
    
    remainingCredits = parseFloat(credits) - parseFloat(amount);

    //console.log("remainingCredits: " + remainingCredits);

    $('#submitBtn').on('click', function () {
        
        //console.log("click");
        
        $("#submitBtn").attr("disabled", true);

        nextLanguage();
    });
})

function nextLanguage() {

    if (ind < languages.length) {
        language = languages[ind].trim();
        displayLanguage = displayLanguages[ind];

//        console.log("language: " + language);
//        console.log("displayLanguage: " + displayLanguage);

        $("#" + language + "").html("Processing");

        translate();

        ind++;
    } else {
        
        //process payment

        //console.log("Translations completed");

        var url = "<a href=\"document-translations.php?did=" + encodeURIComponent(did) + "\">View Translations</a>";

        $("#resultDiv").html(url);
        
        processCredits();        
    }
}

function processCredits() {

    //console.log("processCredits");


    //var displayLang = $("#sourceLanguage option:selected").text();  

    //console.log("uid: " + uid + " did: " + did + " lang: " + lang);

    var form_data = new FormData();
    //form_data.append('input', input);
    form_data.append('remainingCredits', remainingCredits);
    form_data.append('uid', uid);

    //console.log("remainingCredits: " + remainingCredits + " uid: " + uid);


    $.ajax({
        url: 'processCredits.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log(php_script_response);
        }
    });
}

function translate() {

    //console.log("translate");

    var uid = $('#uid').val();

    //var displayLang = $("#sourceLanguage option:selected").text();  

    //console.log("uid: " + uid + " did: " + did + " language: " + language);

    var form_data = new FormData();
    //form_data.append('input', input);
    form_data.append('language', language);
    form_data.append('displayLanguage', displayLanguage); 
    form_data.append('did', decodeURIComponent(did));
    form_data.append('uid', uid);

    //console.log("language: " + language + " displayLanguage: " + displayLanguage + " did: " + did + " uid: " + uid);


    $.ajax({
        url: 'IBM-translate-bulk2.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("response: "+php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var translationid = obj.translationid;            

            if (translationid != "none") {
                $("#" + language + "").html("Completed");
            } else {
                 $("#" + language + "").html("Error");
            }
            nextLanguage();

        }
    });
}
