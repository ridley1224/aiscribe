// JavaScript Document


$(document).ready(function () {

    //console.log("ready2");

    $('#edit_button').on('click', function () {

        //alert("get file");

        $("#docContentDiv").css("display", "none");
        $("#editTxt").css("display", "block");
        $("#cancel_button").css("display", "block");
        $("#save_button").css("display", "block");
        $("#edit_button").css("display", "none");
    });

    $('#cancel_button').on('click', function () {

        //alert("get file");

        $("#docContentDiv").css("display", "block");
        $("#editTxt").css("display", "none");
        $("#cancel_button").css("display", "none");
        $("#save_button").css("display", "none");
        $("#edit_button").css("display", "block");
    });

    //    $('#upload').on('click', function() {
    //        
    //        upload();
    //    });

})

function upload() {

    var file = $('#SELECT_FILE').prop('files')[0];
    var fileType = file["type"];
    var validImageTypes = ["audio/mp3", "audio/mpeg", "audio/wav", "audio/x-wav", "audio/ogg", "audio/flac"];
    var fileExtensions = ['ogg'];
    var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();
    //var pre = $('#fileField').val().split('.')[0].toLowerCase();

    //console.log("ext: " + ext);
    //console.log("client fileType: " + fileType);

    if ($.inArray(fileType, validImageTypes) < 0 && ($.inArray(ext, fileExtensions) < 0)) {

        // invalid file type code goes here.

        $('#uploadStatus').html("Invalid file type. Only audio formats .mp3, .wav, .ogg and .flac files are accepted.");
    } else {
        var file_data = $('#SELECT_FILE').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);

        //$('#uploadStatus').html("File sent");
        $('#uploadStatus').html(file["name"] + ": transcription in progress");
        $('#SELECT_FILE').val('');

        //alert(form_data);     

        $.ajax({
            url: 'upload.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log(php_script_response);

                if (php_script_response == "file uploaded successfully. conversion successful" || php_script_response == "file uploaded successfully. ") {
                    $('#uploadStatus').html(file["name"] + ": Complete");
                }

                //alert(php_script_response); // display response from the PHP script, if any
            }
        });
    }
}


