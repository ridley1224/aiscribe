// JavaScript Document

var timer;
var mid;
var cid;
var done = false;
var code;
var time;
var timerStarted;

$(document).ready(function () {

    //console.log("go");
    
    inactivityTime(); 

    code = $("#code").val();
    mid = getUrlParameter('mid');
    cid = getUrlParameter('cid');

    if (cid != null && mid != null) {

        refreshModelStatus();
    }    
})

var inactivityTime = function () {
    
    window.onload = resetTimer;
    // DOM Events
    document.onmousemove = resetTimer;
    document.onkeypress = resetTimer;
};

function stopRefresh() {
    
    done = true;
    timerStarted = false;
    clearTimeout(timer);
}

function resetTimer() {
    
    clearTimeout(time);
    
    if(!done)
    {
        time = setTimeout(stopRefresh, 60000 * 5);
    }
    
    // 1000 milliseconds = 1 second
}

function closeTip() {

    $("#tipDiv").css("display", "none");
}

function openTip(status1) {

    //console.log("open tip " + status1);

    if (status1 == "pending") {
        $("#tipTxt").html("<strong>Pending</strong> indicates that the model was created. It is waiting for valid training data from a corpora to be added to finish analyzing data.");
    } else if (status1 == "ready") {
        $("#tipTxt").html("<strong>Ready</strong> indicates that the model contains valid data and is ready to be trained.");
    } else if (status1 == "failed") {
        $("#tipTxt").html("<strong>Failed</strong> indicates that training of the model failed. Examine the words in the model's words resource to determine the errors that prevented the model from being trained.");
    } else if (status1 == "available") {
        $("#tipTxt").html("<strong>Available</strong> indicates that the model is trained and ready to use with a recognition request.");
    } else if (status1 == "training") {
        $("#tipTxt").html("<strong>Training</strong> indicates that the model is being trained on data.");
    } else if (status1 == "upgrading") {
        $("#tipTxt").html("<strong>Upgrading</strong> indicates that the model is being upgraded.");
    } else if (status1 == "pending1") {
        $("#tipTxt").html("The model is currently analyzing new corpus data.");
    }

    $("#tipDiv").css("display", "block");
}

function trainModel() {

    //console.log("trainModel " + mid);

    var form_data = new FormData();
    form_data.append('cid', cid);

    $.ajax({
        url: 'IBM-train-model.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            if (php_script_response == "model trained successfully") {
                //start timer for 10 seconds
            }

            //console.log("docData: " + php_script_response);
        }
    });
}

function refreshModelStatus() {

    //console.log("refreshModelStatus");

    var form_data = new FormData();
    form_data.append('cid', cid);

    //console.log("customizationid " + cid);

    $.ajax({
        url: 'IBM-model-info.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("response: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);
            var status = obj.status;
            var timeSince = parseInt(obj.timeSince);

            var statusHTML;

            if (status == "pending") {

                if (timeSince < 60) {
                    //console.log("time less than 20 seconds");
                    statusHTML = status + "<img src='img/tip.png' id='tipIcon6' class='tipIcon6' style='height: 20px; width: 20px;cursor: pointer;'>";
                } else {
                    //console.log("time more than 20 seconds");

                    statusHTML = "<a href='add-corpus.php?cid=" + encodeURIComponent(cid) + "'>" + status + "</a><img src='img/tip.png' id='tipIcon0' class='tipIcon0' style='height: 20px; width: 20px;cursor: pointer;'>";
                }

            } else if (status == "ready") {
                //statusHTML = status + "<img src='img/tip.png' style='height: 20px; width: 20px;' id='tipIcon1'><br><a href='train.php?train=1&cid="+cid+"' class='train'>train</a>"
                statusHTML = status + "<img src='img/tip.png' style='height: 20px; width: 20px;cursor: pointer;' id='tipIcon1'><br><a href='view-model.php?t=1&cid=" + encodeURIComponent(cid) + "&mid=" + encodeURIComponent(mid) + "' class='train'>train</a>";
            } else if (status == "failed") {
                statusHTML = status + "<img src='img/tip.png' style='height: 20px; width: 20px;' id='tipIcon2' class='tipIcon2'>";

            } else if (status == "available") {
                statusHTML = "<a href='transcribe.php?cid=" + encodeURIComponent(cid) + "&l=" + encodeURIComponent(code) + "'>available</a><img src='img/tip.png' style='height: 20px; width: 20px;cursor: pointer;' id='tipIcon3' class='tipIcon3'>";
            } else if (status == "training") {
                statusHTML = status + "<img src='img/tip.png' style='height: 20px; width: 20px;cursor: pointer;' id='tipIcon4' class='tipIcon4'>";
            } else if (status == "upgrading") {
                statusHTML = status + "<img src='img/tip.png' style='height: 20px; width: 20px;cursor: pointer;' id='tipIcon5' class='tipIcon5'>";
            } else {
                statusHTML = status;
            }

            $('#statusCell').html(statusHTML);

            $('#closeTip').on('click', function () {

                closeTip();
            });

            $('.tipIcon0').on('click', function () {

                openTip("pending");
            });

            $('.tipIcon1').on('click', function () {

                openTip("ready");
            });

            $('.tipIcon2').on('click', function () {

                openTip("failed");
            });

            $('.tipIcon3').on('click', function () {

                openTip("available");
            });

            $('.tipIcon4').on('click', function () {

                openTip("training");
            });
            $('.tipIcon5').on('click', function () {

                openTip("upgrading");
            });
            $('.tipIcon6').on('click', function () {

                openTip("pending1");
            });

            if (status != "available" && status != "ready" && status != "failed") {

                //console.log("start timer");

                if (done == false) {

                    timerStarted = true;
                    inactivityTime();
                    timer = setTimeout(runTimer, 10000);
                }
            } else {
                //console.log("stop timer");
                done = true;
                timerStarted = false;
                clearTimeout(timer);
            }
        }
    });
}

function runTimer() {

    //console.log("run it");
    refreshModelStatus();
}
