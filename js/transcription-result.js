// JavaScript Document

var filename;
var selectedLanguages;
var displayLanguages;
var did;
var uid;
var timer;
var ext;

$(document).ready(function () {

    //console.log("ready2");

    did = $('#did').val();
    uid = $('#uid').val();

    $('#docContentDiv').html("");

    $('#pdf-icon').css('cursor', 'pointer');
    $('#word-icon').css('cursor', 'pointer');
    $('#txt-icon').css('cursor', 'pointer');

    $('#edit_button').on('click', function () {

        $("#docContentDiv").css("display", "none");
        $("#editTxt").css("display", "block");
        $("#cancel_button").css("display", "block");
        $("#save_button").css("display", "block");
        $("#edit_button").css("display", "none");
    });

    $('#cancel_button').on('click', function () {

        $("#docContentDiv").css("display", "block");
        $("#editTxt").css("display", "none");
        $("#cancel_button").css("display", "none");
        $("#save_button").css("display", "none");
        $("#edit_button").css("display", "block");
    });

    //console.log("register click");

    $('#save_button').on('click', function () {

        updateTranscription();
    });

    $('#pdf-icon').on('click', function () {

        createPDF();
    });

    $('#word-icon').on('click', function () {

        createWord();
    });

    $('#txt-icon').on('click', function () {

        createTxt();
    });

    $("#checkoutBtn").on('click', function () {

        //console.log("click");

        selectedLanguages = [];
        displayLanguages = [];

        $.each($("input[name='CheckboxGroup1']:checked"), function () {
            selectedLanguages.push($(this).attr("id"));
            displayLanguages.push($(this).attr("alt"));
        });

        if (selectedLanguages.length > 0) {
            //console.log("selected languages: " + selectedLanguages.join(", "));
            //console.log("display languages: " + displayLanguages.join(", "));

            setLanguages();
        }
    });

    getData();
})

function setLanguages() {

    //console.log("setLanguages");

    //    var did = $('#did').val();
    //    var uid = $('#uid').val();

    //console.log("uid: " + uid + " selectedLanguages: " + selectedLanguages);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('languages', selectedLanguages);
    form_data.append('displayLanguages', displayLanguages);

    //form_data.append('did', did);

    $.ajax({
        url: 'setSessionLanguages.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("languageData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var status = obj.status;

            //console.log("status: "+status);

            if (status == "languages set successfully") {
                window.location.href = "confirm-translations.php?did=" + encodeURIComponent(did);
            }
        }
    });
}

function getData() {

    //console.log("getData");

    //    var did = $('#did').val();
    //    var uid = $('#uid').val();

    //console.log("uid: " + uid + " did: " + did);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('did', did);

    $.ajax({
        url: 'docData.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("docData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var response = obj.response.stripSlashes();
            var confidence = obj.confidence.stripSlashes();
            filename = obj.filename;
            //var filename = obj.filename;

            $('#headerLbl').html("Edit Transcription: " + filename);

            if (confidence != "") {
                $('#confidenceDiv').html("Transcription Confidence: " + confidence + "%");
            } else {
                $('#confidenceDiv').html("");
            }

            $('#docContentDiv').html(response);
            $('#editTxt').val(response);

            getLanguages();

        }
    });
}

function getLanguages() {

    //console.log("getLanguages");

    //console.log("uid: " + uid + " did: " + did);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('did', did);

    $.ajax({
        url: 'getLanguages.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("language data: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            $.each(obj, function (index, value) {

                //console.log(value["code"] + " " + value["translated"]);
                //console.log("count");

                var translated = value["translated"];
                var boxstate;
                var color;

                if (translated == 1) {
                    boxstate = "disabled";
                    color = " style=\"background-color:#999\"";
                } else {
                    boxstate = "";
                    color = "";
                }

                var html = "<div><div style=\"float: left;width: 25px;margin-left: 10px;\"><input type=\"checkbox\" id=\"" + value["code"].toString() + "\" alt=\"" + value["displayname"].toString() + "\" name=\"CheckboxGroup1\" " + boxstate + color + "></div><div style=\"margin-bottom:10px;margin-top:10px\">" + value["displayname"].toString() + "</div></div>";

                $('#listBox').append(html);
                //dataArray.push([value["yourID"].toString(), value["yourValue"] ]);
            });
        }
    });
}

function createPDF() {

    //console.log("createPDF");

    var content = $('#docContentDiv').html();
    var did = $('#did').val();
    //var filename = $('#filename').val();

    //console.log("content: " + content);
    //console.log("filename: " + filename);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('content', content);
    form_data.append('did', did);
    form_data.append('filename', filename);

    $.ajax({
        url: 'create-pdf.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("pdfData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var response = obj.output;
            var filename2 = obj.filename;

            if (response.indexOf("created successfully") >= 0) {

                //console.log("open pdf");
                window.open("uploads/" + filename2, '_blank');

                ext = "pdf";
                manageFile();
            }
        }
    });
}

function createWord() {

    //console.log("createWord");

    var content = $('#docContentDiv').html();
    var did = $('#did').val();
    //var filename = $('#filename').val();

    //console.log("content: " + content);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('content', content);
    form_data.append('did', did);
    form_data.append('filename', filename);

    $.ajax({
        url: 'create-word.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("pdfData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var response = obj.output;
            var filename2 = obj.filename;

            if (response.indexOf("created successfully") >= 0) {

                //console.log("open word doc");
                window.open("uploads/" + filename2, '_blank');

                ext = "docx";
                manageFile();
            }
        }
    });
}

function createTxt() {

    //console.log("createTxt");

    var content = $('#docContentDiv').html();
    var did = $('#did').val();
    //var filename = $('#filename').val();

    //console.log("content: " + content);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('content', content);
    form_data.append('did', did);
    form_data.append('filename', filename);

    $.ajax({
        url: 'create-txt.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("pdfData: " + php_script_response);

            var obj = jQuery.parseJSON(php_script_response);

            var response = obj.output;
            var filename2 = obj.filename;

            if (response.indexOf("created successfully") >= 0) {

                //console.log("open txt doc");
                window.open("uploads/" + filename2, '_blank');

                ext = "txt";
                manageFile("txt");
            }
        }
    });
}

function updateTranscription() {

    //console.log("updateTranscription");

    var output = $('#editTxt').val();
    var uid = $('#uid').val();
    var did = $('#did').val();

    //    //console.log("output: " + output);
    //    //console.log("uid: " + uid + " did: " + did);

    if (output != "") {
        //var file_data = $('#SELECT_FILE').prop('files')[0];   
        var form_data = new FormData();
        form_data.append('output', output);
        form_data.append('uid', uid);
        form_data.append('did', did);

        //console.log("form_data: "+form_data);

        $.ajax({
            url: 'update-transcription.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log("response: " + php_script_response);

                if (php_script_response == "transcription updated successfully") {
                    //$('#uploadStatus').html(file["name"] + ": Complete");

                    var newContent = $('#editTxt').val();

                    $('#docContentDiv').html(newContent);
                    $("#docContentDiv").css("display", "block");
                    $("#editTxt").css("display", "none");
                    $("#cancel_button").css("display", "none");
                    $("#save_button").css("display", "none");
                    $("#edit_button").css("display", "block");
                }
            }
        });
    }
}

function manageFile() {

    timer = setTimeout(clearFile, 3000);
}

function clearFile() {

    //console.log("clear file");

    clearTimeout(timer);

    //console.log("updateTranscription");

    var uid = $('#uid').val();
    var did = $('#did').val();

    //    //console.log("output: " + output);
    //console.log("uid: " + uid + " did: " + did);

    //var file_data = $('#SELECT_FILE').prop('files')[0];   
    var form_data = new FormData();
    form_data.append('uid', uid);
    form_data.append('did', did);
    form_data.append('ext', ext);

    //console.log("form_data: "+form_data);

    $.ajax({
        url: 'manage-file.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {

            //console.log("error: " + error);
        },
        success: function (php_script_response) {

            //console.log("response: " + php_script_response);
        }
    });
}
