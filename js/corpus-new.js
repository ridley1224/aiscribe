// JavaScript Document

var lang = "";
var fileName = "";
var hasFile = false;
var hasModel = false;

$(document).ready(function () {

    //console.log("ready3");

    $("#transcribeBtn").css("background-color", "gray");

    $('#transcribeBtn').on('click', function () {

        //alert("get file");

        if (hasFile && hasModel) {
            
            //console.log("upload corpus");

            upload();
        }
    });

    $("#SELECT_FILE").on('change', function () {

        var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();

        if ($.inArray(ext, ['txt']) == -1) {

            //$("#fileNameTxt").html("Invalid file type");
            $("#transcribeBtn").css("background-color", "gray");
            $('#languageTxt').html("Invalid file type. Only text format .txt files are accepted.");

            hasFile = false;

        } else {

            hasFile = true;
            $("#transcribeBtn").css("background-color", "rgb(155,197,61)");
            readURL(this);
            
        }
    });
    
    $("#sourceLanguage").on('change', function () {
        
        //console.log("customizationid: " + $("#sourceLanguage").val());
        
        if($("#sourceLanguage").val() != "Choose custom model...")
        {
            $("#sourceLanguage").css("background-color", "rgba(248,248,248,1.0)");
            hasModel = true;
            
            if(hasFile)
            {
                $("#transcribeBtn").css("background-color", "rgb(155, 197, 61)");
            }
        }
        else
        {
            $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");
            $("#transcribeBtn").css("background-color", "gray");
            hasModel = false;
        }
    })

    $("#selectFileBtn").on('click', function () {

        //console.log("click div");
        $("#SELECT_FILE").click();
    });
    
    var cid = getUrlParameter('cid');

    if (cid) {
        
        //console.log("has model");
        hasModel = true;
    }
})

var readURL = function (input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            fileName = input.files[0].name;

            //console.log("file: " + input.files[0].name);
            $("#fileNameTxt").html(fileName);
            $('#languageTxt').html("");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function upload() {

    var file = $('#SELECT_FILE').prop('files')[0];

    if (file == null) {
        $('#languageTxt').html("Please select a file");
        return;
    }

    var uid = $('#uid').val();
    var customizationid = $('#sourceLanguage').val();
    //var displayLang = $("#sourceLanguage option:selected").text();

    
    var fileType = file["type"];
    var validImageTypes = ["text/txt","text/plain"];

    var ext = $('#SELECT_FILE').val().split('.').pop().toLowerCase();

    //var pre = $('#fileField').val().split('.')[0].toLowerCase();

    //console.log("ext: " + ext);
    //console.log("client fileType: " + fileType);

    var form_data = new FormData();

    if (lang == "Choose custom model...") {

        $('#languageTxt').html("Please select model");
        $("#sourceLanguage").css("background-color", "rgba(255,0,0,0.3)");

        return;
        
    } else {
        
        $('#languageTxt').html("");
        $("#sourceLanguage").css("background-color", "rgba(248,248,248,1.0)");
    }

    if ($.inArray(fileType, validImageTypes) < 0) {

        // invalid file type code goes here.

        $('#languageTxt').html("Invalid file type. Only text formats .txt files are accepted.");

    } else {
        
        form_data.append('file', file);
        //form_data.append('lang', lang);
        //form_data.append('displayLang', displayLang);
        //form_data.append('fileType', fileType);
        form_data.append('filename', fileName);
        //form_data.append('ext', ext);
        form_data.append('customizationid', customizationid);
        
        
        //console.log("fileName: " + fileName);
        
        //console.log("customizationid: " + customizationid);
        
        //return;

        form_data.append('uid', uid);
        //form_data.append('did', did);
        //form_data.append('input', input);

        //$('#SELECT_FILE').val('');

        $.ajax({
            url: 'IBM-create-corpus.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            error: function (xhr, status, error) {

                //console.log("error: " + error);
            },
            success: function (php_script_response) {

                //console.log(php_script_response);
                
                if (php_script_response.indexOf("corpus created successfully") >= 0) {
                    
                    window.location.href = "my-corpora.php";
                }
            }
        });
    }
}