<?php

//$devStatus = "dev";

require_once( 'Connections/transcribe.php' );
include( "functions.php" );
include( "en-de.php" );

date_default_timezone_set( 'America/Detroit' );
$date = date( "Y-m-d H:i:s" );

if ( isset( $_GET[ 'uid' ] ) ) {

    mysql_select_db( $database_transcribe, $transcribe );
    $query_rsUsernameInfo = sprintf( "SELECT userid,email,accountconfirmed FROM users WHERE userid = %s", GetSQLValueString( de($_GET[ 'uid' ]), "int" ) );
    $rsUsernameInfo = mysql_query( $query_rsUsernameInfo, $transcribe )or die( mysql_error() );
    $row_rsUsernameInfo = mysql_fetch_assoc( $rsUsernameInfo );
    $totalRows_rsUsernameInfo = mysql_num_rows( $rsUsernameInfo );
    
//    echo "query: {$query_rsUsernameInfo}<br>";
//    echo "db: {$database_transcribe}";

    if ( $totalRows_rsUsernameInfo > 0 ) {

        if($row_rsUsernameInfo['accountconfirmed'] != 1)
        {
            $insertSQL = sprintf( "UPDATE users SET accountconfirmed = %s, dateconfirmed = %s WHERE userid = %s",
            GetSQLValueString( mysql_real_escape_string( "1" ), "text" ),
            GetSQLValueString( mysql_real_escape_string( $date ), "date" ),
            GetSQLValueString( mysql_real_escape_string( de( $_GET[ 'uid' ] ) ), "int" ) );

            mysql_select_db( $database_transcribe, $transcribe );
            $Result1 = mysql_query( $insertSQL, $transcribe )or die( mysql_error() );
            
            $status = "Your account has been confirmed successfully!";
        
            //send welcome email

            $message = "Thank you joining the AIScribe community. Get started now uploading audio files and get searchable, editable transcripts in minutes. And translate them to your choice of 22 languages. Like magic.";

            $to = $row_rsUsernameInfo['email'];
            $subject = "Welcome to AIScribe";
            $html = $message;
            $text = $message;
            $from = "noreply@myaiscribe.com";        

            include("send-email.php");

            //new user signup email

            $message = "New user {$row_rsUsernameInfo['email']} confirmed account";

            $to = "aiscribedev@gmail.com";
            $subject = "AIScribe signup";
            $html = $message;
            $text = $message;
            $from = "noreply@myaiscribe.com";

            include("send-email.php");
        }
        else
        {
            $status = "Your account has already been confirmed successfully.";
        }
    }
    else
    {
        $status = "Account confirmed error.";
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="boilerplate.css">
    <link rel="stylesheet" href="maintenance.css">
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
    <title>Account Confirmation - AIScribe</title>
</head>

<body>
    <div id="primaryContainer" class="primaryContainer clearfix">
        <div id="headerBG" class="clearfix">
            <span style="font-size:30px;cursor:pointer"><img id="navIcon" name="navIcon" src="img/Hamburger_icon.png" class="image"/></span>

            <a href="index.php"><img id="logo" src="img/logo.png" class="image"/></a>
        </div>
        <div id="titleDiv" class="clearfix">
            <div id="headerTxtBG" class="clearfix">
                <p id="headerLbl">Account Confirmation</p>
            </div>
        </div>
        <div id="contentBG" class="clearfix">
            <div id="renderContent"></div>
            <p id="docContentDiv"><?php echo $status; ?>

            </p>
        </div>
    </div>
</body>
</html>