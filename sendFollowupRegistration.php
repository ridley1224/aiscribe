<?php

require_once( 'Connections/transcribe.php' );
include( "functions.php" );
include( "en-de.php" );


if ( ( isset( $_POST[ "MM_insert" ] ) ) && ( $_POST[ "MM_insert" ] == "form1" ) ) {

    //$_POST[ 'support' ] = "ridleytech@gmail.com";

    $email = $_POST[ 'support' ];

    mysql_select_db( $database_transcribe, $transcribe );
    $query_rsUsernameInfo = sprintf( "SELECT userid,email FROM users WHERE email = %s", GetSQLValueString( $email, "text" ) );
    $rsUsernameInfo = mysql_query( $query_rsUsernameInfo, $transcribe )or die( mysql_error() );
    $row_rsUsernameInfo = mysql_fetch_assoc( $rsUsernameInfo );
    $totalRows_rsUsernameInfo = mysql_num_rows( $rsUsernameInfo );

    if ( $totalRows_rsUsernameInfo > 0 ) {

        $uid = urlencode( en( $row_rsUsernameInfo[ 'userid' ] ) );

        $to = $email;
        $subject = "AIScribe Account Registration Request";
        $html = "We’ve emailed a verification link to {$email}. Click <a href='https://www.myaiscribe.com/confirmation.php?uid={$uid}'>here</a> to finish setting up your account.";
        $text = "We’ve emailed a verification link to {$email}. Click here to finish setting up your AIScribe account.";
        $from = "noreply@aiscribe.com";

        include( "send-email.php" );


        $message = "New user registered from email {$email}";

        $to = "aiscribedev@gmail.com";
        $subject = "AIScribe registration";
        $html = $message;
        $text = $message;
        $from = "noreply@myaiscribe.com";

        include( "send-email.php" );
    

        $status = "follow up sent to {$email}";
    }
}



?>


<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="boilerplate.css">
    <link rel="stylesheet" href="maintenance.css">
    <title>Resend Registration - AIScribe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
</head>

<body>
    <div id="primaryContainer" class="primaryContainer clearfix">
        <div id="headerBG" class="clearfix">
            <span style="font-size:30px;cursor:pointer"><img id="navIcon" name="navIcon" src="img/Hamburger_icon.png" class="image"/></span>
            <a href="index.php"><img id="logo" src="img/logo.png" class="image"/></a>
        </div>
        <div id="titleDiv" class="clearfix">
            <div id="headerTxtBG" class="clearfix">
                <p id="headerLbl">Send Registration Link</p>
            </div>
        </div>
        <div id="contentBG" class="clearfix">
            <form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">

                <p>&nbsp;</p>

                <?php if(isset($status) && (strpos($status, 'follow up sent') !== false)) { 

                echo $status;


                } else { ?>


                <table width="100%" cellpadding="5" cellspacing="5">
                    <tbody>
                        <?php if(isset($status)) { ?>
                        <tr>
                            <td width="93%" style="color: red">
                                <?php echo $status; ?>
                            </td>
                        </tr>

                        <?php } ?>
                        <tr>
                            <td>Insert user email</td>
                        </tr>
                        <tr>
                            <td><textarea name="support" class="support" id="support"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="hidden" name="MM_insert" value="form1">
                                <input type="submit" name="submit" id="submit" value="Submit">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <?php } ?>
            </form>
        </div>
    </div>
</body>
</html>